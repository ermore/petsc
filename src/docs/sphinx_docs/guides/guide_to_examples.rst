================================
Guide to PETSc Tutorial Examples
================================

PETSc has a great many tutorial examples, found in ``tutorials/``
directories throughout the ``src/`` tree. Since these do not have informative
names and vary greatly in their complexity and level of internal documentation,
this page provides guides to connect the tutorial examples to particular physics, 
discretizations, solvers, etc. that you might be interested in.

(Note: there are also examples in ``tests/`` directories, but
these are typically sparsely documented and will only be useful to advanced users.)

.. toctree::
  :maxdepth: 2

  guide_to_examples_by_physics
  physics/guide_to_stokes
